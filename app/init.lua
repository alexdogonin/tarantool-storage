local http_server = require('http.server')
local log = require('log')
local handler = require('app.handler')

local app = {}

function app.init(addr, port, req_interval) 
    box.once('storage_init', function()
        box.schema.space.create('values', {format = {
            name = 'string',
            value = 'any'
        }})
    
        box.space['values']:create_index('values_primary', {
            unique = true,
            parts = {1, 'string'}
        })
    end)

    local server = http_server.new(addr, port, {log_requests = true})
    server:route( { path = '/' }, function(req) return {body='OK'} end)
    server:route( { path = '/kv', method = "POST" }, handler.handlePostValue)
    server:route( { path = '/kv/:key', method = "PUT" }, handler.handlePutValue)
    server:route( { path = '/kv/:key', method = "GET" }, handler.handleGetValue)
    server:route( { path = '/kv/:key', method = "DELETE" }, handler.handleDeleteValue)

    log.info('app starting listen at '..tostring(addr)..':'..tostring(port))
    server:start()
    log.info('app started')

    app.server = server
end

return setmetatable({}, {
    __index = app,
    __newindex = function() 
        error("app: attempt to modify module") 
    end
})