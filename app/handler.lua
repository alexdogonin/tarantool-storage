local log = require('log')
local json = require('json')

local handler = {}

local constants = setmetatable({}, {
  __index = {
    messageWrongBody = "wrong body",
    messageKeyAlreadyExist = "key already exist",
    messageKeyNotFound = "key not found",
    messageEmptyBody = "empty body",
    messageEmptyKey = "empty key",
    messagePostValueSuccess = "post value success",
    messagePutValueSuccess = "put value success",
    messageGetValueSuccess = "get value success",
    messageDeleteValueSuccess = "delete value success"
  },
  __newindex = function() 
    error("app.handler: attempt to modify constant")
  end
})

function handler.handlePostValue(req)
  local body = req:read()
  if 0 == #body then 
    return {status = 400, body = constants.messageEmptyBody}
  end

  log.debug('body: '..tostring(body))

  local data = json.decode(body)
  if (nil == data.value) or (nil == data.key) then 
    log.error(constants.messageWrongBody)

    return {status = 400, body = constants.messageWrongBody}
  end

  if 0 == #data.key then 
    log.error(constants.messageEmptyKey)
    return {status = 400, body = constants.messageEmptyKey}
  end

  if nil ~= box.space['values']:get{data.key} then 
    log.error(constants.messageKeyAlreadyExist)

    return {status = 409, body = constants.messageKeyAlreadyExist}
  end

  box.space['values']:insert{data.key, data.value}

  log.info(constants.messagePostValueSuccess)
  return {status = 200}
end


function handler.handlePutValue(req)
  local key = req:stash('key')
  if 0 == #key then 
    log.error(constants.messageEmptyKey)
    return {status = 400, body = constants.messageEmptyKey}
  end

  local body = req:read()
  log.debug('body: '..tostring(body))

  local value = json.decode(body).value
  if nil == value then 
    log.error(constants.messageWrongBody)
    
    return {status = 400, body = constants.messageWrongBody}
  end

  if nil == box.space['values']:get{key} then 
    log.error(constants.messageKeyNotFound)

    return {status = 404, body = constants.messageKeyNotFound}
  end

  box.space['values']:update(key, { {'=', 2, value} })

  log.info(constants.messagePutValueSuccess)
  return {status = 200}
end


function handler.handleGetValue(req)
  local key = req:stash('key')
  if 0 == #key then 
    log.error(constants.messageEmptyKey)
    return {status = 400, body = constants.messageEmptyKey}
  end

  local turple = box.space['values']:get{key} 
  if nil == turple then 
    return {status = 404, body = constants.messageKeyNotFound}
  end

  log.info(constants.messageGetValueSuccess)
  return {status = 200, body = turple[2]}
end

function handler.handleDeleteValue(req)
  local key = req:stash('key')
  if 0 == #key then 
    log.error(constants.messageEmptyKey)
    return {status = 400, body = constants.messageEmptyKey}
  end

  if nil == box.space['values']:delete(key) then 
    return {status = 404, body = constants.messageKeyNotFound}
  end

  log.info(constants.messageDeleteValueSuccess)
  return {status = 200}
end

return handler