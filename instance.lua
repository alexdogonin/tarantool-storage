require('strict').on()
local os = require('os')

box.cfg{
    log_level = 7;
}
  
function create_users()
    box.schema.user.create('default', { password = 'secret' })
    box.schema.user.grant('default', 'read,write,execute', 'universe')
end

box.once('create_users', create_users)

addr = os.getenv('ADDRESS')
port = os.getenv('PORT')

app = require('app')
app.init(addr, port)