FROM tarantool/tarantool

ADD ./instance.lua /etc/tarantool/instances.enabled/instance.lua
ADD ./app /usr/share/tarantool/app

EXPOSE 5501

ENV ADDRESS=0.0.0.0
ENV PORT=5501

CMD tarantool /etc/tarantool/instances.enabled/instance.lua